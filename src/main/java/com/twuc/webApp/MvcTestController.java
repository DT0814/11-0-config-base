package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MvcTestController {
    @Value("${mvc.str}")
    private String res;
    @Autowired
    Environment environment;
    @Autowired
    DemoBean demoBean;
    @GetMapping("/mvc/test")
    public String test() {
        return environment.getProperty("mvc.str");
    }

    @GetMapping("/mvc/test2")
    public String test2() {
        return demoBean.getName()+demoBean.getAge();
    }
}
