package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PropertyTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void test() throws Exception {
        //Arrange

        //Act

        //Assert
        mockMvc.perform(get("/mvc/test"))
                .andExpect(status().isOk())
                .andExpect(content().string("default"));
    }

    @Test
    void test2() throws Exception {
        //Arrange

        //Act

        //Assert
        mockMvc.perform(get("/mvc/test2"))
                .andExpect(status().isOk())
                .andExpect(content().string("zhangSan18"));
    }
}
